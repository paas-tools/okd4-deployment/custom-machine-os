# Custom Machine OS

This repository contains instructions and build manifests to build a customize "Machine OS" image.
This is the image used by the nodes of an OKD cluster.

This can be used to:

* install older/newer kernels
* install additional packages
* apply special configurations on the node
* ...

The `Dockerfile` "RUN" instructions will depend on what we are trying to do.

The "FROM" instruction should reference the upstream Machine OS image which we want to build upon.
To get the correct image reference, extract the `clusterVersion` string from `okd4-install/chart/values.yaml` and then run:

```sh
oc adm release info $CLUSTER_VERSION --image-for fedora-coreos
```

See upstream documentation for more details and examples:

* <https://docs.okd.io/4.14/post_installation_configuration/coreos-layering.html>
* <https://github.com/coreos/layering-examples>
