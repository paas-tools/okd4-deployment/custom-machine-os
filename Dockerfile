# Original machine OS image for OKD 4.14
FROM quay.io/openshift/okd-content@sha256:0351023b7ac334780e4e9a07d8f732d9a6b7004807e6acf4467520b5e651aa57


# Rollback kernel to 6.4.15 (same as OKD 4.13)
# see https://gitlab.cern.ch/webservices/webframeworks-planning/-/issues/1335
RUN set -x && \
    rpm-ostree override replace https://kojipkgs.fedoraproject.org/packages/kernel/6.4.15/200.fc38/x86_64/kernel-{,core-,modules-,modules-core-}6.4.15-200.fc38.x86_64.rpm && \
     /usr/libexec/rpm-ostree/wrapped/dracut --no-hostonly --kver 6.4.15-200.fc38.x86_64 --reproducible -v --add ostree -f "/usr/lib/modules/6.4.15-200.fc38.x86_64/initramfs.img" && \
    rpm-ostree cleanup -m && \
    ostree container commit
